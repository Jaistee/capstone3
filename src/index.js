import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-day-picker/lib/style.css';
import * as serviceWorker from './serviceWorker';
import App from './App';
// import Events from './views/Events/Events'

// import Login from './views/Users/Login'
// import Register from './views/Users/Register'
// import {Footer} from './views/Layout'

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
