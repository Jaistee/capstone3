import React, {useState, useEffect} from 'react';
import {EventSection, BookingForm} from './components';
import axios from 'axios';
import moment from 'moment';
import {NavigationBar, Footer} from '../Layout';

const Events = () => {

    const [events, setEvents] = useState([]);
    const [showBookingForm, setShowBookingForm] = useState(false);
    const [date, setDate] = useState("");
    const [client, setClient] = useState("");
    const [instructions, setInstructions] = useState("")
    const [party, setParty] = useState("");

    const handleRefresh = () => {
        setParty("")
        setDate("")
    }

    const handleShowBookingForm = () => {
        setShowBookingForm(!showBookingForm);
    }

    const handleAddBooking = () =>{
        axios.post('https://still-sierra-00426.herokuapp.com/addBooking',{
            photoEvent: party.name,
            date: date,
            client: client,
            amount: party.price,
            instructions: instructions
        }).then(res=>{
            console.log(res.data)
            handleShowBookingForm()
            handleRefresh()
        })
    }

    const handleDayClick = (day) => {
        setDate(moment(day).format("MMM DD YYYY"))
        console.log(day)
	}

    const handleSetEventDetails = (party) => {
        setParty(party)
        console.log(party)
        handleShowBookingForm()
    }

    const handleSetInstructions = (e) => {
        setInstructions(e.target.value)
        console.log(e.target.value)
    }

    useEffect(()=>{
		if(sessionStorage.token){
			let loggedInUser = JSON.parse(sessionStorage.user)
			let fullName = loggedInUser.firstName + " " + loggedInUser.lastName
			setClient(fullName);
		}
		axios.get('https://still-sierra-00426.herokuapp.com/showEvents').then(res=>{
            setEvents(res.data)
            console.log(res.data)
		})
	}, []);

    return (
        <React.Fragment>
            <NavigationBar />
            <div className="events-container">
                <h1 className="text-center text-header pt-5">Events</h1>
                {events.map(indivEvent=>
                <EventSection
                    key={indivEvent._id}
                    indivEvent={indivEvent}
                    handleSetEventDetails={handleSetEventDetails}
                    handleShowBookingForm={handleShowBookingForm}
                />
                )}
                <BookingForm 
                    showBookingForm={showBookingForm}
                    handleShowBookingForm={handleShowBookingForm}
                    handleAddBooking={handleAddBooking}
                    handleDayClick={handleDayClick}
                    handleSetInstructions={handleSetInstructions}
                    party={party}
                    handleRefresh={handleRefresh}
                />
            </div>
            <Footer />
        </React.Fragment>
    )
}

export default Events;