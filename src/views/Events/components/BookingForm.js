import React from 'react';
import {Modal, ModalHeader, ModalBody, Button} from 'reactstrap';
import {FormInput, Calendar} from '../../../globalcomponents';

const BookingForm = (props) => {

	const party = props.party;

	const handleCancelBooking = () => {
		props.handleRefresh();
		props.handleShowBookingForm();
	}

	return(
		<React.Fragment>
			<Modal
				isOpen={props.showBookingForm}
				toggle={handleCancelBooking}
			>
				<ModalHeader
					toggle={handleCancelBooking}
					className="text-header bg-danger"
				>
					Book an event
				</ModalHeader>
				<ModalBody
					className="text-center"
				>
					<h1 className="text-header">{party.name}</h1>
					<p className="modal-text">Price: {party.price}</p>
					<Calendar 
						handleDayClick={props.handleDayClick}
					/>
					<FormInput 
						label={"instructions"}
						type={"text"}
						name={"instructions"}
						placeholder={"Enter special instructions here"}
						onChange={props.handleSetInstructions}
					/>
			        <Button
						color="success"
						onClick={props.handleAddBooking}
						className="mx-1"
					>Book Now</Button>
					<Button
						color="danger"
						onClick={handleCancelBooking}
						className="mx-1"
					>
					Cancel
					</Button>
				</ModalBody>
			</Modal>
		</React.Fragment>
	)
}

export default BookingForm;