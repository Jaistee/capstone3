import React from 'react';
import {Button} from 'reactstrap'
import BookingForm from './BookingForm';

const EventSection = (props) => {

    const indivEvent = props.indivEvent;

    return (
        <React.Fragment>
                <div className="events-sub-container">
                    <div className="events-card">
                        <img src={process.env.PUBLIC_URL + indivEvent.image} className="events-image"/>
                        <div className="ml-3">
                            <h3>{indivEvent.name}</h3>
                            <p>{indivEvent.description}</p>
                            <p>Amount: {indivEvent.price}</p>
                            <div className="d-flex justify-content-center">
                                <Button
                                    color="success"
                                    onClick={()=>props.handleSetEventDetails(indivEvent)}
                                    className="text-btn"
                                >Book Now</Button>
                            </div>
                        </div>
                    </div>
                </div>
        </React.Fragment>
    )
}

export default EventSection;