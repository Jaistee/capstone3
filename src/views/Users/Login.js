import React, { useState } from "react";
import { FormInput } from "../../globalcomponents";
import { Button } from "reactstrap";
import axios from "axios";
import {NavigationBar, Footer} from '../Layout'

const Login = () => {
  
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");

    const handleEmailChange = (e) =>{
        setEmail(e.target.value)
        console.log(e.target.value)
    }

    const handlePasswordChange = e =>{
        setPassword(e.target.value)
        console.log(e.target.value)
    }

    const handleLogin = () => {
        axios.post('https://still-sierra-00426.herokuapp.com/login',{
            email: email,
            password: password
        }).then(res=>{
          // can only save string in storage
          sessionStorage.token = res.data.token;
          //JSON.stringify converts data into string
          sessionStorage.user = JSON.stringify(res.data.user);
          // for local storage
          // localStorage.token = res.data.token;
          console.log(res.data.token);

          window.location.replace('#/events');
        });
    }

  return (
    <React.Fragment>
      <NavigationBar />
      <div className="login-container">
        <div className="login-container2">
          <div className="col-lg-4 offset-lg-1 pb-3">
          <h1 className="text-center pt-5 text-header">Login</h1>
            <FormInput
              label={"Email"}
              placeholder={"Enter Your Email"}
              type={"email"}
              onChange={handleEmailChange}
            />
            <FormInput
              label={"Password"}
              placeholder={"Enter Your Password"}
              type={"password"}
              onChange={handlePasswordChange}
            />
            <Button 
                block 
                color="success text-btn"
                onClick={handleLogin}
                >
              Login
            </Button>
          </div>
        </div>
      </div>
      <Footer/>
    </React.Fragment>
  );
};

export default Login;