import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand
} from 'reactstrap'

const NavigationBar = () => {

	const [user,setUser] = useState("")
	const [isOpen, setIsOpen] = useState(false);
	
	const toggle = () => setIsOpen(!isOpen);

	const logout = () =>{
		sessionStorage.token = "";
	}

	useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user);
			setUser(user);
		}
    }, [])
	
	return(
		<React.Fragment>
			<Navbar color="dark" dark expand="md">
				<NavbarBrand href="/" className="navbar-logo">Moments</NavbarBrand>
				<NavbarToggler onClick={toggle}/>
				<Collapse isOpen={isOpen} navbar className="navbar-toggler">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item active my-auto mx-2">
							<Link to="/" className="navbar-link">Home</Link>
						</li>
						<li className="nav-item my-auto mx-2">
							<Link to="/events" className="navbar-link">Events</Link>
						</li>
						{sessionStorage.user ? 
						<React.Fragment>
							<li className="nav-item my-auto mx-2">
								<Link to="/bookings" className="navbar-link">Bookings</Link>
							</li>
							<li className="nav-item my-auto mx-2">
								<Link to="/login" className="navbar-link">Hello {user.firstName}</Link>
							</li>
							<li className="nav-item my-auto mx-2">
								<Link to="/login" className="navbar-link" onClick={logout}>Logout</Link>
							</li>
						</React.Fragment>
						: ""}
						{!sessionStorage.user ?
						<React.Fragment>
							<li className="nav-item my-auto mx-2">
								<Link to="/login" className="navbar-link">Login</Link>
							</li>
							<li className="nav-item my-auto mx-2">
								<Link to="/register" className="navbar-link">Register</Link>
							</li>
						</React.Fragment>
						: ""}
					</ul>
				</Collapse>
			</Navbar>
		</React.Fragment>
	)
}

export default NavigationBar;