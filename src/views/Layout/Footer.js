import React from 'react';

const Footer = () => {
	return(
		<React.Fragment>
			<div className="bg-dark d-flex justify-content-center align-items-center">
				<p className="text-footer">Made with love by Jaypee</p>
			</div>
		</React.Fragment>
	)
}

export default Footer;