import React from 'react';
import {Button} from 'reactstrap';

const BookingRow = (props) => {

    const booking = props.booking;

    return(
        <React.Fragment>
            <tr>
                <td>{booking.refNumber}</td>
                <td>{booking.photoEvent}</td>
                <td>{booking.date}</td>
                <td>{booking.client}</td>
                <td>{booking.amount}</td>
                <td>{booking.paymentDate}</td>
                <td>{booking.instructions}</td>
                <td>
                    <Button
                        color="info"
                        onClick={()=>props.handleSetBooking(booking)}
                        className="m-1"
                    >Update</Button>
                    <Button
                        color="danger"
                        onClick={()=>props.handleDeleteBooking(booking._id)}
                        className="m-1"
                    >Delete</Button>
                </td>
            </tr>
        </React.Fragment>
    )
}

export default BookingRow;