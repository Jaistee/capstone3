import React from 'react';
import {Modal, ModalHeader, ModalBody, Button} from 'reactstrap';
import {Calendar, FormInput} from '../../../globalcomponents';

const BookingForm = (props) => {

	const booking = props.indivBooking;

	const handleCancelBooking = () => {
		props.handleRefresh();
		props.handleShowBookingForm();
	}

	return(
		<React.Fragment>
			<Modal
				isOpen={props.showBookingForm}
				toggle={handleCancelBooking}
			>
				<ModalHeader
					toggle={handleCancelBooking}
					className="text-header bg-danger"
				>
					Book an event
				</ModalHeader>
				<ModalBody
					className="text-center"
				>
					<h1 className="text-header">{booking.photoEvent}</h1>
					<p className="modal-text">Paid Amount: {booking.amount}</p>
                    <p className="modal-text">Current Date: {booking.date}</p>
					<Calendar 
						handleDayClick={props.handleDayClick}
					/>
					<FormInput 
						label={"instructions"}
						type={"text"}
						name={"instructions"}
						placeholder={"Enter special instructions here"}
						value={booking.instructions}
						onChange={props.handleSetInstruction}
					/>
			        <Button
						color="info"
						onClick={props.handleUpdateBooking}
					>Update</Button>
					<Button
                        color="danger"
						onClick={handleCancelBooking}
					>
                    Cancel
					</Button>
				</ModalBody>
			</Modal>
		</React.Fragment>
	)
}

export default BookingForm;