import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {BookingRow, UpdateBookings} from './components';
import moment from 'moment';
import {NavigationBar, Footer} from '../Layout'

const Bookings = () => {

    const [bookings, setBookings] = useState([]);
    const [indivBooking, setIndivBooking] = useState("");
    const [showBookingForm, setShowBookingForm] = useState(false);
    const [date, setDate] = useState("");
    const [instructions, setInstructions] = useState("");

    const handleShowBookingForm = () => {
        setShowBookingForm(!showBookingForm)
    }

    const handleRefresh = () => {
        setDate("");
        setInstructions("");
		setIndivBooking("");
	}

    const handleDeleteBooking = (bookingId) => {
		axios.delete('https://still-sierra-00426.herokuapp.com/deleteBooking/'+bookingId).then(res=>{
			let index = bookings.findIndex(booking=>booking._id===bookingId);
			let newBookings = [...bookings];
			newBookings.splice(index,1);
			setBookings(newBookings);
		})
	}

    const handleDayClick = (day) => {
        setDate(moment(day).format("MMM DD YYYY"))
        console.log(day)
	}

    const handleInstructions = (e) => {
        setInstructions(e.target.value)
    }

    const handleUpdateBooking = (bookingId) => {
		axios.patch('https://still-sierra-00426.herokuapp.com/userUpdateBooking/'+ bookingId, {
            date: date,
            instructions: instructions
		}).then(res=>{
			let index = bookings.findIndex(bookingId=>bookingId._id === bookingId)
			let newBookings = [...bookings];
			newBookings.splice(index,1,res.data)
			setBookings(newBookings);
			handleRefresh();
		})
	}

    const handleSetBooking = (booking) => {
        setDate(booking.date);
        setInstructions(booking.instructions);
        handleShowBookingForm();
    }

	useEffect(()=>{

		if(sessionStorage.token){

			let loggedInUser = JSON.parse(sessionStorage.user)
			console.log(sessionStorage.user)
			if(loggedInUser.role === "Admin"){
				axios.get('https://still-sierra-00426.herokuapp.com/showBookings').then(res=>{
					setBookings(res.data);
				})
			}else{
				let client = loggedInUser.firstName + " " + loggedInUser.lastName
				console.log(client)
				axios.get('https://still-sierra-00426.herokuapp.com/showBookingsByUser/'+client).then(res=>{
					setBookings(res.data)
					console.log(res.data)
				})
			}

		}else{
			window.location.replace('#/login')
		}

    }, []);
    
    return (
        <React.Fragment>
        <NavigationBar />
            <div className="bookings-container">
                <div className="col-lg-10 offset-lg-1">
                        <UpdateBookings 
                            handleRefresh={handleRefresh}
                            showBookingForm={showBookingForm}
                            handleUpdateBooking={handleUpdateBooking}
                            handleSetBooking={handleSetBooking}
                            handleDeleteBooking={handleDeleteBooking}
                            indivBooking={indivBooking}
                        />
                    <h1 className="text-header text-center py-5">Bookings</h1>
                    <table className="table table-striped border text-center">
                        <thead>
                            <tr>
                                <th>Reference Number</th>
                                <th>Event</th>
                                <th>Date</th>
                                <th>Client</th>
                                <th>Payment</th>
                                <th>Payment Date</th>
                                <th>Instructions</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {bookings.map(booking=>
                                <BookingRow 
                                    key={booking._id}
                                    booking={booking}
                                    handleShowBookingForm={handleShowBookingForm}
                                    handleDeleteBooking={handleDeleteBooking}
                                    handleUpdateBooking={handleUpdateBooking}
                                    handleSetBooking={handleSetBooking}
                                    handleRefresh={handleRefresh}
                                    handleDayClick={handleDayClick}
                                    handleInstructions={handleInstructions}
                                />
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
            <Footer />
        </React.Fragment>
    )
}

export default Bookings;