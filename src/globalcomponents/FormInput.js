import React from 'react';
import {FormGroup, Label, Input} from 'reactstrap';

const FormInput = (props) => {
   return(
       <React.Fragment>
           <FormGroup>
               <Label className="form-label-color">{props.label}</Label>
               <Input
                   name={props.name}
                   type={props.type}
                   placeholder={props.placeholder}
                   onChange={props.onChange}
                   defaultValue={props.defaultValue}
                //    style={props.required ? {border: 'solid 1px red'} : null}
                   onBlur={props.onBlur}
               />
           </FormGroup>
       </React.Fragment>
   )
}
export default FormInput;