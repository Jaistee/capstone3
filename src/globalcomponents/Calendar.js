import React from 'react';
import DayPicker from 'react-day-picker';

const Calendar = (props) => {

	// for disabling certain days of the week
	// const disabledDays = {
	// 	daysOfWeek: [0,6]
	// }

	// for disabling all past dates
	const past = {
		before: new Date()
	}

	return(
		<React.Fragment>
			<div className="d-flex justify-content-center">
				<DayPicker 
					onDayClick={props.handleDayClick}
					showOutsideDays
					disabledDays={past}
				/>
			</div>
		</React.Fragment>
	)
}

export default Calendar;