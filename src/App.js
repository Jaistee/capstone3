import React from 'react';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';

const Register = React.lazy(()=>import('./views/Users/Register'));
const Login = React.lazy(()=>import('./views/Users/Login'));
const Events = React.lazy(()=>import('./views/Events/Events'));
const Bookings = React.lazy(()=>import('./views/Bookings/Bookings'))
// const Home = React.lazy(()=>import('./views/Layout/Home'));

const loading = () => {
    return(
      <div className="bg-warning d-flex justify-content-center align-items-center">
          <h1>Loading...</h1>
      </div>
    )
}

const App = () =>{
  return (
      <HashRouter>
          <React.Suspense fallback={loading()}>
              <Switch>
                  <Route
                      path="/"
                      exact
                      name="Login"
                      render={props => <Login {...props} />}
                  >
                  </Route>
                  <Route
                      path="/events"
                      exact
                      name="Events"
                      render={props => <Events {...props} />}
                  ></Route>
                  <Route
                      path="/bookings"
                      exact
                      name="Bookings"
                      render={props => <Bookings {...props} />}
                  ></Route>
                  <Route
                      path="/register"
                      exact
                      name="Register"
                      render={props => <Register {...props} />}
                  >
                  </Route>
                  <Route
                      path="/login"
                      exact
                      name="Login"
                      render={props => <Login {...props} />}
                  >
                  </Route>
              </Switch>
          </React.Suspense>
      </HashRouter>
  )
}

export default App;